var mongoose = require('mongoose');
var db;

var MONGOHQ_URL=
    "mongodb://user:pass@widmore.mongohq.com:10000/GraphamDB";
//"mongodb://user:pass@widmore.mongohq.com:10010/GraphamLocal";



exports.connect = function(db) {
    var options = {
        server:{
            "auto_reconnect": true,
            "poolSize": 5,
            "socketOptions": { 
                "keepAlive": 1,
                connectTimeoutMS: 5000 
            }
        }
    };
    db = mongoose.connect(MONGOHQ_URL, options);
};


// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});


module.exports.Comment = require('./models/comment');
module.exports.Journal = require('./models/journal');
module.exports.Dream = require('./models/dream');
module.exports.User = require('./models/user');





/*
findの仕方
db.Data.findById(req.session.author_id, function(err, data) {
        if(err || !data){
            console.log("error");
            next();
            return;
        }
        
        res.render('editUserpage.ejs',{
            title: "wakeup - top",
            message:req.param.message,
            session:req.session,
            data: data
        });
        req.param.message = null;
        return;
    });
    req.body.password = req.params('password')
    req.query
*/