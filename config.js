/*
appのコンフィグにあたる部分
*/

var express = require('express');
var path = require('path');

module.exports = function(app) {

  // all environments
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  //app.use(express.favicon());

  app.use(express.favicon(__dirname + '/public/favicon.ico', {
    maxAge: 2592000000
  }));

  //middleware?の設定
  app.use(express.logger('dev'));
  app.use(express.bodyParser({uploadDir: './public'}));//Post を使用可能に
  app.use(express.methodOverride());

  //csrf対策
  //TODO!
  //app.use(express.csrf());
  app.use(express.cookieParser('2wsxzaq1'));
  app.use(express.session({secret: '12qwaszx'}));


  app.use(express.compress({level: 1}))//なんか軽くするらしい
  app.use(app.router);//入れてもいれなくても良い。優先順位を変えられるらしい
  app.use(express.static(path.join(__dirname, 'public')));

  // development only
  if ('development' == app.get('env')) {
    app.use(express.errorHandler());
  }

};