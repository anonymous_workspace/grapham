


var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';



/*
/a/u/timeline?
*/

app.get(APIURI+USERURI+'/timeline',function(req,res,next){
    db.User.findOne({_id:req.session._id}).populate('').exec(function(err, data) {
        if(err || !data) return res.send({result:false});
        
        var users = data.follow;
        users.push(data._id);
        var skip = req.query && req.query.skip ? req.query.skip:0;
        
        db.Journal.find({user:{$in:users}},{},{limit:20,skip:skip, sort:{createdAt:-1}}).populate('dream user comments').exec(function(err,journals){
            if (err||!journals) return res.send({result:false});
            db.User.populate(journals, {
                path: 'comments.user',
                select:'displayName _id username'
            }, function (err,journals){    
                return res.send({result:true,journals:journals});
            });
        });
        
        
        
        
        
    });
});


/*******************************************************************************************

RESTful

*******************************************************************************************/


//get
app.get(APIURI+USERURI+'/journal',function(req,res,next){
    return res.redirect(APIURI+'/'+req.session._id+'/journal');
});


//create
app.post(APIURI+USERURI+'/journal',function(req,res,next){
    var aJournal = {
        user:req.session._id,
        dream:req.body.dream,
        displayTitle:req.body.displayTitle,
        body:req.body.body
    }
    db.Journal.create(aJournal,function(err,journal){
        if(err){
            return next(err);
        } 
        return res.send({result:true,journal:journal});
    });
});


//delete
app.del(APIURI+USERURI+'/journal/:journalId',function(req,res,next){
    console.log("delete journal");
    db.Journal.findOne({_id:req.params.journalId},function(err,journal){
        journal.delete(function(err){
            if (err) return res.send({result:false});
            return res.send({result:true});
        })
    });
});
