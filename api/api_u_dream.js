


var express = require('express');
var db = require('../db');
var http = require('http');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';

//get
app.get(APIURI+USERURI+'/dream',function(req,res,next){
    return res.redirect(APIURI+'/'+req.session._id+'/dream');
});



//create
app.post(APIURI+USERURI+'/dream',function(req,res,next){
    var aDream = {
        user:req.session._id,
        displayTitle:req.body.displayTitle
    }
    db.Dream.create(aDream,function(err,dream){
        if(err){
            return res.send({result:false});
        }
        
        //自動的にeventタイプのジャーナルも作成する
        var aJournal = {
            user:req.session._id,
            dream:dream._id,
            displayTitle:"",
            type:"event",
            body:"startDream"
        }
        console.log(aJournal);
        db.Journal.create(aJournal,function(err,journal){
            if(err){
                return res.send({result:false});
            } 
            return res.send({result:true,dream:dream});
        });
        
    });
});


app.del(APIURI+USERURI+'/dream/:dreamId',function(req,res,next){
    
    db.Dream.findOne({_id:req.params.dreamId},function(err,dream){
        if(err || !dream){
            return res.send({result:false});
        }
        dream.delete(function(err){
            if(err) return res.send({result:false});
            return res.send({result:true});
        });        
    });
});


/*
    var api_req = https.request({host:'graph.facebook.com', path: api_path, method:'GET'}, function(api_res){
        api_res.setEncoding('utf8');
        api_res.on('data',function(d){
            //これはもう形式的にこう言う感じ
            data += d
        });
        api_res.on('end', function(){
            //読み込みが完了
            friends = JSON.parse(data);
            res.render('index', {locals:{title: req.user.displayName,user_id:req.user.id, friends_list: friends }});
        });
    });
    api_req.end();
    */


//friendlistを取得する方法
/*
app.get('/', function(req,res,next){
    if (!passport.session.accessToken){
        //ログインしてないので、facebookのログインページに飛ばす
        res.redirect("/");
    }else{
        next();
    }
},function(req, res,next){
    var api_path = '/'+req.user.id+'/friends?access_token='+passport.session.accessToken;
    console.log(req.user);
    var data = "";
    var friends;
    var api_req = https.request({host:'graph.facebook.com', path: api_path, method:'GET'}, function(api_res){
        api_res.setEncoding('utf8');
        api_res.on('data',function(d){
            //これはもう形式的にこう言う感じ
            data += d
        });
        api_res.on('end', function(){
            //読み込みが完了
            friends = JSON.parse(data);
            res.render('index', {locals:{title: req.user.displayName,user_id:req.user.id, friends_list: friends }});
        });
    });
    api_req.end();
});*/

