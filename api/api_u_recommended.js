
var express = require('express');
var db = require('../db');
var f = require('../functions.js');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';



/* おすすめのユーザー */


app.get('/a/u/recommended',function(req,res,next){
    var recommended = [];
    db.User.findOne({_id:req.session._id},function(err,user){
        if(err||!user) return res.send({result:false});
        db.User.find({},{},{}).exec(function(err,users){
            if(err||!users) return res.send({result:false});
            
            f.forEach(users,function(auser){
                if(auser._id.toString() == user._id.toString()) return;
                if(f.UserIndexOf(user.follow,auser) == -1){
                    //フォローしてない人をrecommend!
                    recommended.push(auser);
                }
            });
            
            return res.send({result:true,recommended:recommended});
            
        });
    });
});