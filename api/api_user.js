//RESTful


var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';

//userdata
app.get(APIURI+'/:user',function(req,res,next){
    var userid = req.params.user;
    var query =  userid.match(/^[0-9]/) ?  {_id:userid}:{username:userid};
    db.User.findOne(query,function(err,user){     
        if (err || !user) return res.send({result:false});
        return res.send({result:true,user:user});
    });
});

//id only
app.get(APIURI+'/:user/dream',function(req,res,next){
    var userid = req.params.user;
    var query =  {user:userid};
    var skip = req.query && req.query.skip ? req.query.skip:0;
    db.Dream.find(query,{},{limit:20,skip:skip,sort:{createdAt:-1}}).populate('user').exec(function (err,dreams){     
        if (err || !dreams) return res.send({result:false});
        return res.send({result:true,dreams:dreams});
    });
});


app.get(APIURI+'/:user/dream/:dreamId',function(req,res,next){
    var userid = req.params.user;
    var query =  {_id:req.params.dreamId};
    db.Dream.findOne(query,{},{}).populate('').exec(function (err,dream){     
        if (err || !dream) {
            return res.send(404);
        }
        return res.send({result:true,dream:dream});
    });
});

app.get(APIURI+'/:user/dream/:dreamId/journal',function(req,res,next){
    var userid = req.params.user;
    var query =  {dream:req.params.dreamId};
    var skip = req.query && req.query.skip ? req.query.skip : 0;
    db.Journal.find(query,{},{limit:20,skip:skip,sort:{createdAt:-1}}).populate('').exec(function (err,journals){     
        if (err || !journals) return res.send({result:false});
        return res.send({result:true,journals:journals});
    });
});


app.get(APIURI+'/:user/journal',function(req,res,next){
    var userid = req.params.user;
    var query =  {user:userid};
    var skip =  req.query.skip ? req.query.skip:0;
    
    
    
    db.Journal.find(query,{},{limit:20,skip:skip,sort:{createdAt:-1}}).populate('user dream comments').exec(function (err,journals){    
        if (err || !journals) return res.send({result:false});
        
        db.User.populate(journals, {
            path: 'comments.user',
            select:'displayName _id username'
        }, function (err,journals){    
            return res.send({result:true,journals:journals});
        });
    });        
    
});


app.get(APIURI+'/:user/journal/:journalId',function(req,res,next){
    var userid = req.params.user;
    var query =  {_id:req.params.journalId};
    db.Journal.findOne(query,{},{}).populate('comments').exec(function (err,journal){     
        if (err || !journal) return res.send({result:false});
        return res.send({result:true,journal:journal});
    });
});

app.get(APIURI+'/:user/follow',function(req,res,next){
    var userid = req.params.user;
    var query =  userid.match(/^[0-9]/) ?  {_id:userid}:{username:userid};
    db.User.findOne(query,{},{}).populate('follow').exec(function (err,user){  
        if(err || !user) return res.send({result:false});
        return res.send({result:true,follow:user.follow});
    });
});

app.get(APIURI+'/:user/follower',function(req,res,next){
    var userid = req.params.user;
    var query =  userid.match(/^[0-9]/) ?  {_id:userid}:{username:userid};
    db.User.findOne(query,{},{}).populate('follower').exec(function (err,user){  
        if(err || !user) return res.send({result:false});
        return res.send({result:true,follower:user.follower});
    });
});





