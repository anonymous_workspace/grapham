//RESTful


var express = require('express');
var db = require('../db');
var f = require('../functions.js');
var http = require('http');


var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path');


var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';


//get
app.get(APIURI+USERURI,function(req,res,next){
    return res.redirect('/a/' + req.session._id);
});


//create
//a/uはおかしいかな？
// a/u はsessionが無いとアクセスできない
//ログインも実行されます
app.post(APIURI,function(req,res,next){
    var aUser = {
        username:req.body.user.username,
        displayName:req.body.user.displayName,
        facebook:req.body.user.facebook,
        description:req.body.user.description
    }
    db.User.create(aUser,function(err,user){
        if(err){
            return res.send({result:false,error:err});
        } 
        req.session._id = user._id;
        req.session.username = user.username;
        
        return res.send({result:true,user:user});
    });
});



//updata
app.put(APIURI+USERURI,function(req,res,next){
    //何かの処理
    db.User.findOne({_id:req.session._id},function(err,user){
        user.username = req.body.user.username;
        user.displayName = req.body.user.displayName;
        user.description = req.body.user.description;
        
        user.save(function(err){
            if(err) return res.send({result:false});
            return res.send({result:true,user:user});
        });
    });
});





var SAVEDIR_PATH = __dirname + '/../uploaded/';
//delete
app.del(APIURI+USERURI,function(req,res,next){
    
    //アイコンの削除がまっている！
    var targetPath = path.normalize(SAVEDIR_PATH + req.session._id +  ".jpg");
    if (fs.existsSync(targetPath)) {
        //存在してたら削除
        fs.unlink(targetPath,function(err){
            if(err){
            }
        });
    }
    db.User.findOne({_id:req.session._id},function(err,user){
        if(err||!user) return res.send({result:false});
        user.delete(function(err){
            if(err) return res.send({result:false});
            return res.send({result:true});
        });
    });
});