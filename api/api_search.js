//RESTful


var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';


//tostringしたら良くなった.......eeeeee
var index = function (arr, val){
  for (var i=0; i<arr.length; i++) {
    if (arr[i]._id.toString() === val._id.toString())
      return i;
  }
  return -1;
}

//userdata
app.get(APIURI+'/search',function(req,res,next){
  //  var userid = req.params.user;
  //  var query =  userid.match(/^[0-9]/) ?  {_id:userid}:{username:userid};
  //  db.User.findOne(query,function(err,user){     
  //    if (err || !user) return res.send({result:false});
  //    return res.send({result:true,user:user});
  //  });
  console.log("happy!" + req.query.searchWord);

  var result = [];
  //username
  db.User.find({username:new RegExp(req.query.searchWord, "i")}).populate('dreams').exec(function(err,users){
    //以下は配列をpushする場合！
    //Array.prototype.push.apply(result,users);
    result = users;
    //result.push(users);
    //要素をpushする時は以下
    //result.push(user);
    //console.log(result);
    //displayNameで検索
    db.User.find({displayName:new RegExp(req.query.searchWord, "i")}).populate('dreams').exec(function(err,users){
      if(users.length > 0){
        for(var i = 0;i<users.length;i++){
          var user = users[i];
          //既に入ってたら入れない
          if(index(result,user) === -1){
            result.push(user);
          }
        }
      }

      //夢から検索（populateしてから検索できないので逆方向つまりdreamsから検索しますs）
      db.Dream.find({displayTitle:new RegExp(req.query.searchWord, "i")}).populate('user').exec(function(err,dreams){
        if(dreams.length > 0){
          for(var i = 0;i<dreams.length;i++) {
            var dream = dreams[i];
            if(index(result,dream.user)== -1){
              result.push(dream.user);
            }
          }
        }

        return res.send(result);
      });
    }); 
  });
});




