

var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';



/*******************************************************************************************

RESTful

*******************************************************************************************/




/*
createのためには、
req.body.commentBodyと
req.body.journalIdを送ってくること
*/

//create
app.post(APIURI+USERURI+'/comment',function(req,res,next){
    db.Journal.addComment(req.body.journalId,{
        user:req.session._id,
        body:req.body.commentBody
    }, function(err,comment) {
        if (err) return res.send({result:false});
        //commentのユーザーを放り込まなくちゃいけないんだけど、入らない？のでangular側で入れます。
        return res.send({result:true,comment:comment});
    });
});


//delete
app.del(APIURI+USERURI+'/comment/:commentId',function(req,res,next){
    db.Comment.findOne({_id:req.params.commentId},function(err,comment){
        //対応journalからも自動的に削除される
        comment.delete(function(err){
            if (err) return res.send({result:false});
            return res.send({result:true});
        })
    });
});





