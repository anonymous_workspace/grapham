


var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';


app.get(APIURI + USERURI + '/follow',function(req,res,next){
    return res.redirect(APIURI+'/'+req.session._id+'/follow');
});
app.get(APIURI + USERURI + '/follower',function(req,res,next){
    return res.redirect(APIURI+'/'+req.session._id+'/follower');
});




//follow
app.post(APIURI + USERURI + '/follow',function(req,res,next){
    db.User.follow(req.session._id,req.body.followedId,function(err){
        if(err) res.send({result:false});
        return res.send({result:true});
    });
});


//unfollow
app.del(APIURI + USERURI + '/follow',function(req,res,next){
    db.User.unfollow(req.session._id,req.body.unfollowedId,function(err){
        if(err) res.send({result:false});
        return res.send({result:true});
    });
});

