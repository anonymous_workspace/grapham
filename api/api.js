

var express = require('express');

var app = express();
module.exports = app;




app.all(/^\/a($|\/.*$)/, function(req, res, next) {
  /*
cross origin resource sharing
http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
*/
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});


app.all('/a/u',function(req,res,next){
  if(!req.session._id) return res.send({result:false,message:"ログインが必要です"});
  next();
});

app.all('/a/u/*',function(req,res,next){
  if(!req.session._id) return res.send({result:false,message:"ログインが必要です"});
  next();
});

app.use(require('./api_search'));
app.use(require('./api_u'));
app.use(require('./api_u_icon'));
app.use(require('./api_u_dream'));
app.use(require('./api_u_follow'));
app.use(require('./api_u_journal'));
app.use(require('./api_user'));
app.use(require('./api_journal'));
app.use(require('./api_u_recommended'));
app.use(require('./api_u_comment'));