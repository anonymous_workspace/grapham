

var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

const APIURI = '/a';


/* like */
app.post(APIURI+'/journal/:journalId/like', function(req, res, next) {
    db.Journal.like(req.params.journalId, function(err,journal) {
        if (err) return res.send({result:false});
        res.send({result:true});
    })
});
