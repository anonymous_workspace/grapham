

var express = require('express');
//var db = require('../db');
var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path');

var app = express();
module.exports = app;

const APIURI = '/a';
const USERURI = '/u';




//これだと public/iconsに入れちゃいます
// __dirname ってのはこのファイルのフォルダつまり、routes
var SAVEDIR_PATH = __dirname + '/../uploaded/';
app.put(APIURI + USERURI + '/icon',function(req,res,next){
    if (!req.body.dataUrl) return res.send({result:false});
    
    // 保存先ディレクトリがなかったら作成
    var saveDir = path.normalize(SAVEDIR_PATH);
    if (!fs.existsSync(saveDir)) {
        mkdirp.sync(saveDir);
    }
    // 保存先のパスを取得
    var targetPath = path.normalize(saveDir + req.session._id +  ".jpg");
    
    var img = req.body.dataUrl;
    // strip off the data: url prefix to get just the base64-encoded bytes
    var data = img.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(data, 'base64');
    
    //既存のファイルを消してから書き込み
    fs.unlink((targetPath),function(err){             
        fs.writeFile(targetPath, buf,function(err){
            if(err) return res.send({result:false});
            return res.send({result:true});
        });
    });
});