var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
var f = require('../functions.js');



//*************************** Schemaの定義 ***************************//
var DreamSchema = new Schema({
    //オーナーユーザー
    user:{type: ObjectId, ref:'User', required:true},
    title:String,
    displayTitle:String,
    description:String,
    createdAt: {type: Date}
});
//,default:new data.now()ってやるとアクセスするたびに時間が入る



//*************************** 新規作成 ***************************//

//create
DreamSchema.statics.create = function(aDream,callback){
    if (!aDream.user){
        Console.log("新規作成のDreamにuserが設定されていません");
        throw Error;
    }
    var newDream = new this(aDream);
    //この時点でもう_idはついてる！
    newDream.save(function(err){
        callback(err,newDream);
    });
};




//delete
DreamSchema.methods.delete = function(callback){
    //ジャーナルを全て削除
    Journal.find({dream:this._id},function(err,journals){
        f.forEach(journals,function(journal){
            journal.delete(null);
        });
    });
    this.remove(callback);
}

//*************************** 検索関連 ***************************//
DreamSchema.pre('save', function(next) {
    if(this.isNew) {
        this.createdAt = new Date();
    }
    next();
});



//*************************** expots ***************************//


module.exports = mongoose.model('Dream', DreamSchema);


var Journal = require('./journal');

