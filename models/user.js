var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    f = require('../functions.js');


//*************************** Schemaの定義 ***************************//
var UserSchema = new Schema({
    username:{type: String, required: true, index: {unique:true}},
    displayName:String,
    facebook:{
        id:String,
        username:String
    },
    twitter:{
        id:String,
        username:String
    },
    description:String,
    follow:[{type: ObjectId, ref:'User'}],
    follower:[{type: ObjectId, ref:'User'}]
});
//refはmodel名で良いと思うので、Userでいいや


//*************************** 新規作成 ***************************//

//新規作成
//newUserはjson形式のオブジェクトで、nameとか、displaynameとかいろいろもってる！
//callback(err,user){}
UserSchema.statics.create = function(auser,callback){
    //callbackはfunction(err){}
    var newUser = new this(auser);
    newUser.save(function(err){
        callback(err,newUser);
    });
};


//delete
UserSchema.methods.delete = function(callback){
    var self = this;
    
    //dreamを消す（jounarlはdreamに付随して消える）
    Dream.find({user:this._id},function(err,dreams){
        f.forEach(dreams,function(dream){
            dream.delete(null);
        })
    });
    
    User.find({$or: [{follow: self._id}, {follower: self._id}]},function(err,users){
        f.forEach(users,function(user){
            //followの中から削除
            f.UsersPop(user.follow,self);
            //followerの中から削除
            f.UsersPop(user.follower,self);
            user.save(null);
        })
    });
    
    //ここまで    
    self.remove(callback);   
};

//*************************** follow,unfollow ***************************//


//競合してる
UserSchema.methods.dofollow = function(followingId){
    User.follow(this._id,followingId);
};

UserSchema.methods.unfollow = function(unfollowingId){
    User.unfollow(this._id,unfollowingId);
};




var indexOf = function(array,obj){
    for(var i = 0;i<array.length;i++){
        if (array[i] == obj) return i;
    }
    return -1;
}

//arrayはuserの単純配列
var UserIndexOf = function(users,user){
    //idを抽出
    var userId = user._id?user._id:user;
    for(var i = 0;i<users.length;i++){
        if ((users[i]._id ? users[i]._id:users[i]).toString() == userId.toString()) return i;
    }
    return -1;
}


UserSchema.statics.follow = function(followingId,followedId,callback){
    if(followingId === followedId) return ;
    //followingIdがfollowedIdをフォローする
    User.findOne({_id:followingId},function(err,user){
        if(err) callback(err);
        if (UserIndexOf(user.follow,followedId) == -1){
            user.follow.push(followedId);
            user.save(function(err){
                if(err) callback(err);
                User.findOne({_id:followedId},function(err,user){
                    if(err) callback(err);
                    if (UserIndexOf(user.follower,followingId) == -1){
                        user.follower.push(followingId);
                        user.save(callback);
                    }
                });
            });
        }
    });
}

UserSchema.statics.unfollow = function(unfollowingId,unfollowedId,callback){
    if(unfollowingId === unfollowedId) return ;
    //followingIdがfollowedIdをフォローする
    User.findOne({_id:unfollowingId},function(err,user){
        if(err) callback(err);
        f.UsersPop(user.follow,unfollowedId);
        user.save(function(err){
            if(err) callback(err);
            User.findOne({_id:unfollowedId},function(err,user){
                if(err) callback(err);
                
                f.UsersPop(user.follower,unfollowingId);
                user.save(callback);
            });
        });
    });
}



//*************************** 検索関連 ***************************//




module.exports = mongoose.model('User', UserSchema);

var Dream = require('./dream');