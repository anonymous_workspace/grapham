var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
var f = require('../functions.js');




//*************************** Schemaの定義 ***************************//
//ジャーナルのアクセスは_idで良いかな、うん。
var JournalSchema = new Schema({
    user:{type: ObjectId, ref:'User', required:true},
    dream:{type: ObjectId, ref:'Dream', required:true},
    displayTitle:String,
    likeCount: {type: Number, required:true, default:0},
    type: {type: String, required:true, default:"journal"},
    comments:[{type: ObjectId, ref:'Comment'}],
    body:{type: String, required:true},
    createdAt: {type: Date}
});

//*************************** expots ***************************//



//*************************** 新規作成 ***************************//

JournalSchema.statics.create = function(aJournal,callback){
    if (!aJournal.user){
        console.log("新規作成のJournalにuserが設定されていません");
        throw Error;
    }
    if (!aJournal.dream){
        console.log("新規作成のJournalにdreamが設定されていません");
        throw Error;
    }
    var newJournal = new this(aJournal);
    newJournal.save(function(err){callback(err,newJournal)});
};



//delete
JournalSchema.methods.delete = function(callback){
    Comment.find({journal:this._id},function(err,comments){
        if(!err && comments){
            //コメントがあった！
            console.log("comments:" + comments.toString());
            f.forEach(comments,function(comment){
                //deleteは単体削除用として使わない
                comment.remove(null);
            });
        }
    });
    
    this.remove(callback);
}


JournalSchema.pre('save', function(next) {
    if(this.isNew) {
        this.createdAt = new Date();
    }
    next();
});



/* コメントを追加 */
/*
aCommentにはuserとbodyだけ入れれば良い！
*/
JournalSchema.statics.addComment = function(id,aComment,callback){
    this.findOne({_id:id},function(err,journal){
        if(err) return callback(err,null);
        aComment.journal = journal;
        Comment.create(aComment,function(err,comment){
            if(err) return callback(err,null);
            journal.comments.push(comment);
            journal.save(function(err){
                callback(err,comment);
            });
        });
    });
};


/* like */
JournalSchema.statics.like = function(id,callback){
    this.findByIdAndUpdate(id, {$inc:{likeCount:1}}, callback);
    /*
    callback = function(err,journal){};
    */
};


module.exports = mongoose.model('Journal', JournalSchema);


var Comment = require('./comment.js');