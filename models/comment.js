var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;



//*************************** Schemaの定義 ***************************//
//ジャーナルのアクセスは_idで良いかな、うん。
var CommentSchema = new Schema({
    user:{type: ObjectId, ref:'User', required:true},
    journal:{type: ObjectId,ref:'Journal',required:true},
    body:{type: String, required:true},
    createdAt: {type: Date}
});





//*************************** 新規作成 ***************************//

CommentSchema.statics.create = function(aComment,callback){
    if (!aComment.user){
        console.log("新規作成のCommentにuserが設定されていません");
        throw Error;
    }
    if (!aComment.journal){
        console.log("新規作成のCommentにdreamが設定されていません");
        throw Error;
    }
    var newComment = new this(aComment);
    newComment.save(function(err){callback(err,newComment)});
};

//delete
CommentSchema.methods.delete = function(callback){
    //journal.commentsの中から削除
    Journal.findOne({_id:this._id},function(err,journal){
        if(err||!journal) return;
        
        journal.comments.some(function(element,index,array){
            if (element.toString() == this._id.toString()){
                //削除の実行
                array.splice(index,1);
                return true;
            }
            return false;
        });
        
    });
    this.remove(callback);
};

//新規作成日時
CommentSchema.pre('save', function(next) {
    if(this.isNew) {
        this.createdAt = new Date();
    }
    next();
});



//*************************** expots ***************************//


module.exports = mongoose.model('Comment', CommentSchema);

//exportsの後に読み込む事！
var Journal = require('./journal');

