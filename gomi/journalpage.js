var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;


//新規登録
app.post('/user/journal',function(req,res,next){
    console.log(req.body.dream);
    db.Dream.findOne({_id:req.body.dream},function(err,dream){
        if(!dream) next();
        db.Journal.create({
            user:dream.user,
            dream:req.body.dream,
            displayTitle:req.body.displayTitle,
            body:req.body.body
        },function(err,journal){
            if(err||!journal){
                throw err;
            }
            
            dream.journals.push(journal);
            dream.save();
            
            return res.send({result:true,message:null});
        });
    });
});


//削除
app.del('/user/journal/:journalId',function(req,res,next){
    db.Journal.findOne({_id : req.params.journalId},function(err,journal){
        if (err) return next(err);
        if (!journal) return res.send(404);
        
        
        journal.remove(function(err) {
            if (err) return next(err);
            //dreamの側でも削除
            db.Dream.findOne({_id:journal.dream},function(err,dream){
                //populateしてない
                myArray = dream.journals;
                console.log(myArray);
                console.log(journal._id);
                for(i = 0; i < myArray.length; i++){
                    if(myArray[i] === journal._id){
                        myArray.splice(i,1);
                    }
                }
                dream.journals = myArray;
                dream.save();
                
                
                
                return res.send({result:true,message:null});
            });
            
        });
    });
});

