/* フォローに関するプロセス */

var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;



//フォローする
app.post('/user/follow',function(req,res,next){
    
    //フォローする人
    var following = req.session._id;
    var followed  = req.body.followed;
    
    db.User.findOne({_id:following},function(err,user){
        if(err){
            throw err;
        }
        if (user.following.indexOf(followed) == -1){
            user.following.push(followed);
            user.save();
        }
    });
    db.User.findOne({_id:followed},function(err,user){
        if(err){
            throw err;
        }
        if (user.followed.indexOf(following) == -1){
            user.followed.push(following);
            user.save();
        }
    });
    
    return res.send({result:true,message:null});
});

//フォロー解除する
app.post('/user/unfollow',function(req,res,next){
    
    //unfollow
    var unfollowing = req.session._id;
    var unfollowed  = req.body.unfollowed;
    
    db.User.findOne({_id:unfollowing},function(err,user){
        if(err){
            throw err;
        }
        if (user.following.indexOf(unfollowed) != -1){
            //削除する処理
            
            user.following.some(function(followingId, i){
                if (followingId == unfollowed)  user.following.splice(i,1); //id:3の要素を削除
            });
            user.save();
        }
    });
    db.User.findOne({_id:unfollowed},function(err,user){
        if(err){
            throw err;
        }
        if (user.followed.indexOf(unfollowing) != -1){
            
            user.followed.some(function(followedId, i){
                if (followedId == unfollowing)  user.followed.splice(i,1); //id:3の要素を削除
            });
            
            user.save();
        }
    });
    
    return res.send({result:true,message:null});
});