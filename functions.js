
var http = require('http');

/*
便利functions
*/
module.exports = {
    forEach : function(array,callback){
        if(!array) return;
        for(var i=0;i<array.length;i++){
            callback(array[i]);
        }
    },
    
    
    //usersの中にuserが含まれていればpopする
    //usersはobjectIdのみでもpopulateしてあっても大丈夫
    UsersPop : function(users,user){
        var userId = user._id?user._id:user;
        users.some(function(element,index,array){
            if ((element._id ? element._id : element).toString() == userId.toString()){
                array.splice(index,1);
                return true;
            }
            return false;
        });
    },
    //arrayはuserの単純配列
    UserIndexOf : function(users,user){
        //idを抽出
        var userId = user._id?user._id:user;
        for(var i = 0;i<users.length;i++){
            if ((users[i]._id ? users[i]._id:users[i]).toString() == userId.toString()) return i;
        }
        return -1;
    },
    
    selfRequest : function(options,callback){
        var api_req = http.request(options, function(api_res){
            api_res.setEncoding('utf8');
            var data = "";
            api_res.on('data',function(d){data += d;});
            api_res.on('end', function(){
                callback(null,data);
            });
        });
        api_req.on('error', function(e) {
            //console.log('problem with request: ' + e.message);
            callback(e,null)
        });
        api_req.end();
    }
}


//friendlistを取得する方法
/*
app.get('/', function(req,res,next){
    if (!passport.session.accessToken){
        //ログインしてないので、facebookのログインページに飛ばす
        res.redirect("/");
    }else{
        next();
    }
},function(req, res,next){
    var api_path = '/'+req.user.id+'/friends?access_token='+passport.session.accessToken;
    console.log(req.user);
    var data = "";
    var friends;
    var api_req = https.request({host:'graph.facebook.com', path: api_path, method:'GET'}, function(api_res){
        api_res.setEncoding('utf8');
        api_res.on('data',function(d){
            //これはもう形式的にこう言う感じ
            data += d
        });
        api_res.on('end', function(){
            //読み込みが完了
            friends = JSON.parse(data);
            res.render('index', {locals:{title: req.user.displayName,user_id:req.user.id, friends_list: friends }});
        });
    });
    api_req.end();
});*/





//callback(err,data)
/*
var options={
    host:'0.0.0.0',
    path: '/a/user',
    method:'GET'
}
selfRequest(options,function(err,data){
    console.log(data); 
});
*/