// マウスボタンの状態
var mouseDown = false;
// Canvasサイズ
var viewSize = 320;
// トリミングサイズ
var trimSize = 240;
var trimPadding = (viewSize -trimSize)/2;
// 画像描画開始位置
var viewX = trimPadding;
var viewY = trimPadding;
// マウスボタンを押したときの位置
var startX = 0;
var startY = 0;
// 読み込んだイメージのサイズ
var imageWidth;
var imageHeight;

var canvas;
var context;
var img;

// 縮小率
var range;
//retio = 1 で trimsize
var ratio = 1;

// トリム結果の表示先
var trimCanvas;  
var rate;

function setUp() {
    // Canvas
    canvas = document.getElementById('view_canvas');
    canvas.width = viewSize;
    canvas.height = viewSize;
    
    trimCanvas = document.getElementById('trim_canvas');
    trimCanvas.width = trimSize;
    trimCanvas.height = trimSize;
    
    
    // 画像
    img = new Image();
    img.onload = function() {
        context = canvas.getContext('2d');
        imageWidth =  img.width;
        imageHeight = img.height;
        
        rate = imageWidth < imageHeight? imageWidth/trimSize : imageHeight/trimSize;
        draw(trimPadding,trimPadding);
    }
    //読み込んだら現状を表示する
    //img.src = "/icons/<%= data._id %>";
    
    // スライドバー
    range = document.getElementById('image_ratio');
    range.value = 0;
    range.onchange = function() {
        //今のセンター
        var centerX = viewX + (imageWidth/rate * ratio)/2;
        var centerY = viewY + (imageHeight/rate * ratio)/2;
        
        ratio = 1+this.value/50;
        
        //新しい ViewX,Y
        viewX = centerX - (imageWidth/rate * ratio)/2;
        viewY = centerY - (imageHeight/rate * ratio)/2;
        
        // 画像の外まで飛んでいかないように
        if(viewX > trimPadding ) viewX = trimPadding;
        var maxX = trimPadding + trimSize - imageWidth/rate * ratio;
        if(viewX < maxX) {
            viewX = maxX;
        }
        // 画像の外まで飛んでいかないように
        if(viewY > trimPadding ) viewY = trimPadding;
        var maxY = trimPadding + trimSize -imageHeight/rate * ratio
        if(viewY < maxY) {
            viewY = maxY;
        }
        draw(viewX, viewY);
    }
}

// Canvasに選択した画像を展開
function preview(ele) {
    // ファイルが選択されているか？
    if (!ele.files.length) return;
    // Canvas使えるか？
    if ( ! canvas || ! canvas.getContext ) return;
    // 対象型式の画像か？
    var file = ele.files[0];
    if (!/^image\/(png|jpeg|gif)$/.test(file.type)) return;
    
    // リーダー
    var reader = new FileReader();
    reader.onload = function() {
        img.src = reader.result;
    }  
    //読み込み実施
    viewX = trimPadding;
    viewY = trimPadding;
    range.value = 0;
    ratio = 1;
    reader.readAsDataURL(file); 
}

// 画像の表示位置変更
function onCanvasMouseMove(event) {
    if (mouseDown){
        // 横位置計算
        viewX = viewX + (event.clientX - startX) / 1;
        // 縦位置計算
        viewY = viewY + (event.clientY - startY) / 1;
        startX = event.clientX;
        startY = event.clientY
        
        // 画像の外まで飛んでいかないように
        if(viewX > trimPadding ) viewX = trimPadding;
        var maxX = trimPadding + trimSize - imageWidth/rate * ratio;
        if(viewX < maxX) {
            viewX = maxX;
        }
        // 画像の外まで飛んでいかないように
        if(viewY > trimPadding ) viewY = trimPadding;
        var maxY = trimPadding + trimSize -imageHeight/rate * ratio
        if(viewY < maxY) {
            viewY = maxY;
        }
        
        // 再描画
        draw(viewX, viewY);
    }
}

// キャンバス上のマウスボタン押下
function onCanvasMouseDown(event) {
    mouseDown = true;
    startX = event.clientX;
    startY = event.clientY;
}

// キャンバス上のマウスボタン開放
function onCanvasMouseUp() {
    mouseDown = false;
    startX = 0;
    startY = 0;
}

// 画像をキャンバスに描画
function draw(x, y) {
    context.fillStyle = "#FFFFFF";
    context.fillRect(0,0,viewSize,viewSize);
    
    context.drawImage(img, x, y, imageWidth/rate * ratio, imageHeight/rate * ratio);
    context.strokeStyle = 'rgba(0,0,0, .6)';
    context.strokeRect(trimPadding-1, trimPadding-1, trimSize+2 , trimSize+2);
    //context.strokeRect(trimPadding - 2, trimPadding - 2, trimSize + 4, trimSize + 4);
    context.fillStyle = 'rgba(255, 255, 255, 0.7)';
    context.fillRect(0,0,trimPadding-1,viewSize-trimPadding+1);
    context.fillRect(trimPadding-1,0,viewSize,trimPadding-1);
    context.fillRect(0,viewSize-trimPadding+1,viewSize-trimPadding+1,viewSize);
    context.fillRect(viewSize-trimPadding+1,trimPadding-1,viewSize,viewSize);
}

// トリミング実施
function trimImage() {
    var trimData = context.getImageData(trimPadding, trimPadding, trimSize, trimSize);
    var trimContext = trimCanvas.getContext('2d');
    trimContext.putImageData(trimData, 0, 0 );
    //console.log(trimData);
    var trimImageUrl = trimCanvas.toDataURL("image/jpg");
    return trimImageUrl;
}

$(function($) {
    setUp();
});