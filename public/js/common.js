

/*アラートの表示・非表示  */
//アラートを表示する！
function showAlertWithTypeMessage(type,message){
    $('div.main').css('padding-top','71px');
    //$('div.message').css('opacity','1');
    var contents = '<div class="message alert '+type+' fade in"><button type="button"  onClick="$(this).closeAlertBlock();"  class="close">×</button> ' + message + ' \</div>';
    $("div#alertBlock").html(contents);
    //$('div.message').css('opacity','0');
    
}

//アラートを非表示にする
$(function($){
    $.fn.closeAlertBlock = function() {
        //ここにハンドル
        $(".alert").alert('close');
        $('div.main').css('padding-top','20px');
    };
});


var textareaValToHTMLText = function(textareaVal)
{
    var txt = textareaVal;
    // まず改行らしき文字を\nに統一。\r、\r\n → \n
    txt = txt.replace(/\r\n/g, '\n');
    txt = txt.replace(/\r/g, '\n');
    //エスケープ
    txt = htmlspecialchars(txt);
    // 改行を区切りにして入力されたテキストを分割して配列に保存する。
    var lines = txt.split('\n');
    
    var replacedText = lines.join('<br>');
    return replacedText;
}

function htmlspecialchars(ch) { 
    ch = ch.replace(/&/g,"&amp;") ;
    ch = ch.replace(/"/g,"&quot;") ;
    ch = ch.replace(/'/g,"&#039;") ;
    ch = ch.replace(/</g,"&lt;") ;
    ch = ch.replace(/>/g,"&gt;") ;
    return ch ;
}

var HTMLTextToTextAreaVal = function(HTMLText){
    var txt = HTMLText;
    // 改行を区切りにして入力されたテキストを分割して配列に保存する。
    var lines = txt.split('<br>');
    var replacedText = lines.join('\r\n');
    return replacedText;
}


var timeToWords = function(d){
    if(d == "" || d == "undifined" || d == undefined ){
        return "";
    }
    var createdAt = d.toString();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var words = new String();
    words += createdAt.substring(11, 15) + "/" + (months.indexOf(createdAt.substring(4, 7))+1).toString() + "/" + createdAt.substring(8, 10) + " "+ createdAt.substring(16, 21);
    return words;
};

var changeTimeZone = function(d, offset) {
    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
    // return time as a string
    
    //nd = 変更された時間
    return nd;
};

//stringifyされたdateをparseしてDateを返す
var parseDate = function(dateString){
    return new Date(dateString);
};
