


function PostJournalCtrl($scope,$http) {
    $scope.newJournal = {dream:"",displayTitle:"",body:""};
    
    //dreamsがfetchされたら、それの先頭を自動で選択
    $scope.$watch('$parent.sessiondreams', function() {
        if($scope.$parent.sessiondreams && $scope.$parent.sessiondreams.length > 0){
            $scope.newJournal.dream = $scope.$parent.sessiondreams[0]._id;
        }
    },true);
    
    $scope.postNewJournal = function(){
        $http.post('/a/u/journal', {
            dream:$scope.newJournal.dream,
            displayTitle:$scope.newJournal.displayTitle,
            body:textareaValToHTMLText($scope.newJournal.body)
        })
        .success(function(data, status, headers, config) {
            if(data.result){
                
                data.journal.user = $scope.$parent.sessionuser;
                data.journal.createdAt = changeTimeZone(new Date(data.journal.createdAt),9);
                $scope.$parent.sessiondreams.some(function(elem,index,array){
                    if(elem._id == $scope.newJournal.dream){
                        data.journal.dream = elem;
                        return true;
                    }
                    return false;
                });
                if($scope.$parent.timeline.push){
                    $scope.$parent.timeline.push(data.journal);
                }
            }else{
                //失敗
                showAlertWithTypeMessage('alert-warning',"エラーだよん！")
            }
            $('#myModal').modal('hide');
            $scope.newJournal.displayTitle="";
            $scope.newJournal.body="";
        })
        .error(function(data, status, headers, config) {
            //エラーを出力するとか
            $('#myModal').modal('hide');
            showAlertWithTypeMessage('alert-warning',"エラーだよん！")
        });
    }
}


function PostDreamCtrl($scope,$http){
    
    $scope.displayTitle = "";
    $scope.postNewDream = function(){
        
        $http.post('/a/u/dream', {
            displayTitle:$scope.displayTitle
        })
        .success(function(data, status, headers, config) {
            if(data.result){
                data.dream.createdAt = changeTimeZone(new Date(data.dream.createdAt),9);
                $scope.$parent.sessiondreams.push(data.dream);
            }else{
                //失敗
                showAlertWithTypeMessage('alert-warning',"エラーだよん！")
            }
            
            $('#dream-modal').modal('hide'); 
            $scope.displayTitle = "";
        })
        .error(function(data, status, headers, config) {
            //エラーを出力するとか
            $('#myModal').modal('hide');
            showAlertWithTypeMessage('alert-warning',"通信エラーだよん！")
        });
    }
}



function DreamCtrl($scope,$http){
    
    $scope.deleteDream = function(dreamId){
        if (!$scope.$parent.sessionuser._id){
            return;
        }
        $http.post('a/u/dream/'+ dreamId,{_method:"delete"}).
        success(function(data, status, headers, config) {
            //成功
            if(data.result){
                $scope.$parent.sessiondreams.some(function(element, i){
                    if (element._id.toString() == dreamId.toString())  $scope.$parent.sessiondreams.splice(i,1);
                });
            }else{
                showAlertWithTypeMessage('alert-warning',"エラーだよん！")
            }
        }).error(function(data, status, headers, config) {
            showAlertWithTypeMessage('alert-warning',"エラーだよん！")
        });
    }
}





/*
followedIdを取得するために、ejsのテンプレートから data._idを取得しないといけない。
*/



function FollowCtrl($scope,$http){
    $scope.IsFollow = false;
    $scope.$watch('$parent.sessionuser.follow', function() {
        if(!$scope.$parent.sessionuser.follow) return;
        $scope.$parent.sessionuser.follow.some(function(elem,index,array){
            if(elem == $scope.$parent.pageuser._id){
                //フォローしてる！
                $scope.IsFollow = true;
                return true;
            }
            return false;
        });
    },true);
    
    $scope.dofollow = function(){
        if (!$scope.$parent.sessionuser._id){
            showAlertWithTypeMessage('alert-warning',"フォローにはログインが必要です");
            return;
        }
        
        $http.post('a/u/follow',{followedId:$scope.$parent.pageuser._id}).
        success(function(data, status, headers, config) {
            //成功
            if(data.result){
                
                //TODO: followに入っているのがidだけなのかどうか？
                
                $scope.$parent.sessionuser.follow.push($scope.$parent.pageuser._id);
                $scope.$parent.pageuser.follower.push($scope.$parent.sessionuser._id);
                $scope.IsFollow = true;
                showAlertWithTypeMessage('alert-success',"フォローしました！")
            }else{
                showAlertWithTypeMessage('alert-warning',"エラーだよん！")
            }
        }).error(function(data, status, headers, config) {
            showAlertWithTypeMessage('alert-warning',"通信エラーだよん！")
        });
    }
    
    
    $scope.unfollow = function(){
        $http.post('a/u/follow',{_method:"DELETE",unfollowedId:$scope.$parent.pageuser._id}).
        success(function(data, status, headers, config) {
            //成功
            if(data.result){
                
                $scope.$parent.sessionuser.follow.some(function(elem,index,array){
                    if(elem == $scope.$parent.pageuser._id){
                        array.splice(index,1);
                        return true;
                    }
                    return false;
                });
                $scope.$parent.pageuser.follower.some(function(elem,index,array){
                    if(elem == $scope.$parent.sessionuser._id){
                        array.splice(index,1);
                        return true;
                    }
                    return false;
                });
                
                
                
                $scope.IsFollow = false;
                showAlertWithTypeMessage('alert-success',"フォローを解除しました！")
                
            }else{
                showAlertWithTypeMessage('alert-warning',"エラーだよん！")
            }
        }).error(function(data, status, headers, config) {
            showAlertWithTypeMessage('alert-warning',"通信エラーだよん！")
        });
    }
}

