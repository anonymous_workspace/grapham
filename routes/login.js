var express = require('express');
var db = require('../db');
var LocalData = require('../LocalData');
var f = require('../functions');
var passport = require('passport'); //passportのモジュールの宣言
var FacebookStrategy = require('passport-facebook').Strategy //Facebook認証用モジュールのstrategyを利用するための宣言

var testerList = require('./testerlist').testerList;


/*
var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path');

var http = require('http');
*/



var app = express();
module.exports = app;

app.use(passport.initialize()); //passportの初期化
app.use(passport.session()); //passportでのログイン状態を保持するpassport sessionミドルウェアを有効にする

passport.use(new FacebookStrategy({
    clientID: LocalData.FBAppID, //Facebookでアプリを登録し割り振られるID
    clientSecret: LocalData.FBAppSecret, //Facebookでアプリを登録した場合に割り振られるSecret
    callbackURL: LocalData.FBAppCallback //認証後のリダイレクト先URL Facebookでのアプリ登録時に指定したものと同様のURLにすること
},function(accessToken, refreshToken, profile, done){
    //passport.session.accessToken = accessToken; // 認証後返されるaccessTokenをセッションに持たせておく 本当にアプリを作る場合はこのあたりの扱いは注意
    
    //sessionを使わないので app.use(psssport.session); を宣言しないはずなんだけど…
    
    // 以下今回は単純にFacebookから返されるProfileをそのままユーザ情報としてセッションで保持するようにしておく
    process.nextTick(function(){
        //重い処理やる的な？
        //doneはpassportのsessionの中に入れる的な？？
        //こことserializedUserが繋がってるみたいだね
        //ここに渡したもの = req.user らしい
        //ここを改造sるうべし
        done(null,profile);
    });
}));




//どうやら。req.userに投げようとするとserializeが、引っ張るとdesirializeが呼ばれるっぽい
//となるとここは変えなくていいかな
//ここはとtokenには関係ないので、正直変える必要ない気がしてきた…passwordとかも入ってないしな…
//!!!これはpassportのsessionを使うから！！らしい！req.userな気がするけど違うのかな！よくわかんない！
//passportのsession使ってないけど、実行されるみたいなのでこのままにしておきましょう。
passport.serializeUser(function(user, done){done(null, user);});
passport.deserializeUser(function(obj, done){done(null, obj);});

//allはgetもpostもってこと
app.all('/login*',function(req, res, next) {
    // ログインしてればリダイレクト
    if (req.session._id) {
        return res.redirect('/');
    }
    return next();
});

app.get('/login',function(req,res){
    res.render('login.ejs',{
        title: "ログイン - Grapham",
        message:req.param.message,
        session:req.session,
        data: null
    });
    req.param.message = "";
    return;
});

//postでもgetでもログアウト処理してリダイレクト
app.get('/logout',function(req,res){
    req.session.destroy();//coockie を削除
    return res.redirect('/');
});

app.post('/logout',function(req,res){
    req.session.destroy();//coockie を削除
    return res.redirect('/');
});





//PassportのFacebook Strategyでのルートに乗せるための最初のauthenticate関数の呼出をする
//Facebookの認証画面にリダイレクトされてFacebook側のフローに移すので、コールバック関数では何もしない（定義なし）
app.get('/login/facebook', passport.authenticate('facebook'));



//エラーを受け取っていないか調べる
//エラーを受け取っている場合はホームにリダイレクト
app.get('/login/facebook/callback',function(req,res,next){
    if(req.query.error_code){
        req.param.message = "facebookのログインシステムに異常が発生しました";
        return res.redirect('/');
    }
    next();
});

//Facebookでの認証が終わってFacebookからリダイレクトされてくるURLのルート
//もう一度Passportのルートに乗せられて、セッションでの永続化などがされたあとに最終的にどこにルーティングするかのコールバック関数を定義する
//認証成功時は'/'にリダイレクトされる
app.get('/login/facebook/callback',passport.authenticate('facebook',{
    //ログインに失敗
    failureRedirect: '/login'
}),function(req, res,next) {
    console.log("login process");
    db.User.findOne({"facebook.id":req.user.id}).populate('').exec(function(err,user){
        if (err){
            //何かの処理
            req.param.message = "エラーが発生しました。"
            console.log("err");
        }
        
        if (!user){
            //データなかった
            //Signup用のページにリダイレクトするのがいいのかもしれないですけど、良いでしょう！
            
            /*
            //サインアップ可能かチェック
            if(testerList.indexOf(req.user.id) === -1){
                //サインアップできない人は送り出しー
                req.param.message = "申し訳ございませんが現在は招待制となっております。興味のある方は、https://twitter.com/ryutamatsuno までご連絡ください。";
                return res.redirect('/');
            }
            */
            
            //ログインできる！
            return res.redirect('/signup');
            
        }else{
            req.session._id = user._id;
            req.session.username = user.username;
            next();
        }
    });
},function(req, res,next) {
    //ログイン完了
    console.log("Success To Login :"+req.session._id);
    return res.redirect('/');
});




//リダイレクト
app.all('/signup',function(req,res,next){
    // ログインしてればホームへリダイレクト
    if (req.session._id) {
        return res.redirect('/');
    }
    next();
});

//サインアップ
app.get('/signup',function(req,res,next){
    if(!req.user){
        // login経由で来ていない。
        //要修正！！本当なら最初からこのページに呼び込むこと！
        return res.redirect('/login');
    }
    
    
    //サインアップ用画面を用意
    var user = {
        username:req.user.username,
        displayName:req.user.displayName,
        facebook:{
            id:req.user.id,
            username:req.user.username
        },
        description:req.user._json.bio
    };
    
    res.render('signup.ejs',{
        title:"サインアップ - Grapham",
        message:req.param.message,
        session:req.session,
        user:user
    });
    req.param.message = null;
    return;
});




app.post('/signup',function(req,res,next){
    
    
    db.User.create({
        username:req.body.username,
        displayName:req.body.displayName,
        facebook:{
            id:req.user.id,
            username:req.user.username
        },
        description:req.body.description
    },function(err,user){
        if(err){
            console.log("err");
        }
        
        console.log("New user Sign up:"+req.user.username);
        
        //作ったでござる
        //sessionそのものにuser入れちゃうのは良くないらしい
        req.session._id = user._id;
        req.session.username = user.username;
        
        req.user = null;
        
        req.param.message = 'サインアップしました';
        return res.redirect('/');
    });
});



//デバッグ用ログイン
app.get('/login/sample',function(req,res,next){
    db.User.findOne({"username":"sample"}).populate('dreams').exec(function(err,user){
        if (err){
            //何かの処理
            console.log("err");
        }
        
        if (!user){
            //データなかった
            //Signup用のページにリダイレクトするのがいいのかもしれないですけど、良いでしょう！
            
            db.User.create({
                username:"sample",
                displayName:"サンプル",
                facebook:{
                    id:"1s",
                    username:"noname"
                },
                description:"サンプルです"
            },function(err,user){
                if(err){
                    console.log("err");
                }
                
                //作ったでござる
                //sessionそのものにuser入れちゃうのは良くないらしい
                req.session._id = user._id;
                req.session.username = user.username;
                req.session.dreams = user.dreams;
                return res.redirect('/');
            });
            
        }else{
            req.session._id = user._id;
            req.session.username = user.username;
            req.session.dreams = user.dreams;
            return res.redirect('/');
        }
        
    });
});





//friendlistを取得する方法
/*
app.get('/', function(req,res,next){
    if (!passport.session.accessToken){
        //ログインしてないので、facebookのログインページに飛ばす
        res.redirect("/");
    }else{
        next();
    }
},function(req, res,next){
    var api_path = '/'+req.user.id+'/friends?access_token='+passport.session.accessToken;
    console.log(req.user);
    var data = "";
    var friends;
    var api_req = https.request({host:'graph.facebook.com', path: api_path, method:'GET'}, function(api_res){
        api_res.setEncoding('utf8');
        api_res.on('data',function(d){
            //これはもう形式的にこう言う感じ
            data += d
        });
        api_res.on('end', function(){
            //読み込みが完了
            friends = JSON.parse(data);
            res.render('index', {locals:{title: req.user.displayName,user_id:req.user.id, friends_list: friends }});
        });
    });
    api_req.end();
});*/
