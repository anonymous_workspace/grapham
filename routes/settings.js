var express = require('express');
var db = require('../db');
var fs = require("fs");
var mkdirp = require('mkdirp');
var path = require('path');



var app = express();
module.exports = app;

//設定トップ
app.get('/user/settings',function(req, res,next){
    //表示
    db.User.findOne({_id:req.session._id}, function(err, data) {
        if(err || !data){
            console.log("error");
            next();
            return;
        }
        res.render('settings/settings.ejs',{
            title:"設定 - Grapham",
            message:null,
            session:req.session,
            data:data
        });
        req.param.message = null;
        return;
    });
});



