var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;

//ソート用関数
//ascending = 昇順
function sortByKey(array, key,ascending) {
    var anum = ascending? -1 : 1 ;
    return Array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? anum : ((x > y) ? anum * -1 : 0));
    });
}


//dreampage
app.get('/:username/dream/:dreamId',function(req,res,next){
    db.Dream.findOne({_id:req.params.dreamId}).populate('journals').exec(function (err,dream){     
        if (err || !dream) {
            return next();
        }
        db.User.findOne({_id:dream.user}).populate('dreams').exec(function(err,data){
            res.render('dreampage.ejs', {
                title: dream.displayTitle + " -  Grapham",
                message: req.param.message,
                session: req.session,
                pageuser:data,
                dream:dream
            });
        });
        return;
    });
});


