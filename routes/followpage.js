var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;


//フォローしている人
app.get('/:username/follow',function(req,res,next){
    db.User.findOne({username: req.params.username}).populate('follow').exec(function (err, data) {
        if(err||!data){
            return next();
        }
        res.render('followpage.ejs', {
            title:  req.params.username +"のフォロー -  Grapham",
            message: req.param.message,
            session: req.session,
            pageuser:data
        });
        return;
    });
});


//フォローされている人
app.get('/:username/follower',function(req,res,next){
    db.User.findOne({username: req.params.username}).populate('follower').exec(function (err, data) {
        if(err||!data){
            return next();
        }
        res.render('followerpage.ejs', {
            title:  req.params.username +"のフォロワー -  Grapham",
            message: req.param.message,
            session: req.session,
            pageuser:data
        });
        return;
    });
});

