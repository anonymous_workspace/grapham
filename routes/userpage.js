var express = require('express');
var db = require('../db');

var app = express();
module.exports = app;


//ユーザーページ
app.get('/:username', function (req, res, next) {
    //var startTime =  new Date().getTime();
    db.User.findOne({username: req.params.username}).populate('follow follower').exec(function (err, data) {
        if(err||!data){
            return next();
        }
        
        res.render('userpage.ejs', {
            title: " -  Grapham",
            message: req.param.message,
            session: req.session,
            pageuser:data
        });
        req.param.message = null;
        return;
    });
});

/*
app.del('/user',function(req,res,next){
    //req.session_idwのユーザーを削除する
    
    var session_id = req.session._id;
    console.log("delete");
    //ユーザをfined
    db.User.findOne({_id: session_id}).exec(function (err, data) {
        //夢を全部削除
        if(data.dreams.length > 0){
            for(var i = 0;i<data.dreams.length;i++){
                var dream = data.dreams[i];
                db.Dream.findOne({_id:dream},function(err,dream){
                    dream.remove(function(err){
                        if(err) {
                            return next(err);
                        }
                        data.deleteDream(dream,function(err){
                            if(err) return next(err);
                        });
                    });
                });   
            }
        }
        //ジャーナルを全部削除//ユーザーから取ってくるかな
        db.Journal.find({user:session_id}).exec(function (err,journals){
            for(var i = 0;i<journals.length;i++){
                var jounal = journals[i];
                //ジャーナルを削除
                jounal.remove(function(err){
                    if(err){
                        return next(err);
                    }
                });
            }
        });
        
        //削除されるユーザーをフォローしているユーザーからfollowingを削除
        var deletedUserId = session_id;
        
        for (var i=0;i<data.followed.length;i++){
            db.User.findOne({_id:data.followed[i]},function(err,user){
                if(err){
                    throw err;
                }
                //削除されるユーザーをフォローしている人
                if (user.following.indexOf(deletedUserId) != -1){
                    //followingの中からdeletedUserIdを探す
                    user.following.some(function(followingId, i){
                        if (followingId == deletedUserId)  user.following.splice(i,1); //id:3の要素を削除
                    });
                    user.save();
                }
            });
        }
        
        
        //削除されるユーザーがフォローしているユーザーからfollowedを削除
        for (var i=0;i<data.following.length;i++){
            db.User.findOne({_id:data.following[i]},function(err,user){
                if(err){
                    throw err;
                }
                //削除されるユーザーがフォローしている人
                if(!user.followed){return;}
                if (user.followed.indexOf(deletedUserId) != -1){
                    user.followed.some(function(followedId, i){
                        if (followedId == deletedUserId)  user.followed.splice(i,1); //id:3の要素を削除
                    });
                    user.save();
                }
            });   
        }
        
        data.remove(function(err){
            if(err){
                console.log("errr!");
            }
            console.log("data dalete!");
        });
    });
    
    //sessionをクリアして終了！
    return res.redirect('/logout');
});

*/



