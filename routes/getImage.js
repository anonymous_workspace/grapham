var express = require('express');
var db = require('../db');
var fs = require("fs");

var app = express();
module.exports = app;

//**************************** getimage ********************************


var SAVEDIR_PATH = __dirname + '/../uploaded/';

app.get('/icon',function(req,res,next){
    return res.redirect('/Anonymous/icon');
});


//_idでとりにくる
//usernameで取りに行かせたい
app.get('/:_id/icon', function(req, res, next) {
    
    var userid = req.params._id;
    
    if(userid.match(/^[0-9]/)){
        //_idが来てる
        res.locals.work = userid;
        return next();
    }else{
        //usernameが来てる
        db.User.findOne({username:userid},function(err,user){
            if(err|!user){
                res.locals.work  = "Anonymous";
                return next();
            }else{
                console.log(user);
                res.locals.work = user._id;
                return next();
            }
        });
    }
},function(req, res, next) {
    var filePath = SAVEDIR_PATH + res.locals.work + ".jpg";
    req.session.work = null;
    fs.exists(filePath, function (exists) {
        if (exists){
            //jpgファイルがある！
            res.send(fs.readFileSync(filePath), {
                'Content-Type': 'image/jpeg'
            }, 200);
            return
        }
        
        //ない！Anonymous.jpgを返す
        filePath = SAVEDIR_PATH  + "Anonymous.jpg";
        return res.send(fs.readFileSync(filePath), {
            'Content-Type': 'image/jpeg'
        }, 200);
    });
});
