
/**
 * Module dependencies.
 */

//モジュールの読み込み
var express = require('express');
var http = require('http');
var path = require('path');
var db = require('./db');
var mongoose = require('mongoose');
var lwl = require('lwl');
var fs = require('fs');

var app = express();


//*************************** コンフィグ作成 ***************************//

require('./config')(app);



//*************************** ログフォルダ作成 ***************************//

const LOGDIRECTORY = './ErrorLogs';
if(!fs.existsSync(LOGDIRECTORY)){
    fs.mkdirSync(LOGDIRECTORY);
}

//*************************** 接続 ***************************//

db.connect();//dbを接続



/*
var flag = 2;
if(flag ==0 ){
    
    db.User.create({username:"user1"},function(err,user1){});
    db.User.create({username:"user2"},function(err,user2){});
}else if(flag == 1){
    db.User.findOne({username:"user1"}).exec(function(err,user1){
        console.log("1:"+user1);
        db.User.findOne({username:"user2"},function(err,user2){
            console.log("2:"+user2);
            user2.unfollow(user1._id);
            user1.unfollow(user2._id);
        });
    });
}else{
    db.User.findOne({username:"user1"}).exec(function(err,user1){
        console.log("1:"+user1);
        db.User.findOne({username:"user2"},function(err,user2){
            console.log("2:"+user2);
            user2.dofollow(user1._id);
            user1.dofollow(user2._id);
        });
    });
}
*/

//*************************** ルーティング ***************************//

//謎?
app.all('*',function(req,res,next){
    if ( mongoose.connection.readyState == 0 ) { // disconnected
        console.log("接続が切断されました。自動接続を試みます。");
        db.connect();
    }else{
    }
    next();
});



app.use(function(req,res,next){
    //localsの関数にのせる
    res.locals.changeTimeZone = function(d, offset) {
        // convert to msec
        // add local time zone offset
        // get UTC time in msec
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        // create new Date object for different city
        // using supplied offset
        nd = new Date(utc + (3600000*offset));
        // return time as a string
        
        //nd = 変更された時間
        return nd;
    };
    
    res.locals.timeToWords = function(d){
        var createdAt = d.toString();
        console.log("createdAt:" + createdAt);
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        
        var words = new String();
        words += createdAt.substring(11, 15) + "/" + (months.indexOf(createdAt.substring(4, 7))+1).toString() + "/" + createdAt.substring(8, 10) + " "+ createdAt.substring(16, 21);
        console.log("words:"+words);
        return words;
    };
    
    next();
});




// メインのルーティング 
//不確定要素が多いものほど下

//最初にログイン判定
app.all('/user*',function(req,res,next){
    if(!req.session._id){
        //ログインしてない
        return res.redirect('/');
    }
    return next();
});

//初期ページ
app.use(require('./routes/index'));

app.use(require('./routes/login'));
app.use(require('./routes/search'));


app.use(require('./api/api'));


app.use(require('./routes/settings'));
//app.use(require('./routes/follow'));
app.use(require('./routes/dreampage'));
//app.use(require('./routes/journalpage'));

app.use(require('./routes/getImage'));
app.use(require('./routes/userpage'));
app.use(require('./routes/followpage'));


//*************************** not found の処理 ***************************//

/*
//エラーの処理？
app.use(function(err, req, res, next) {
    console.log(err);
    //next();
});

*/

app.use(function(req, res, next){
    console.log(req.host);
    console.log("notfound");
    // the status option, or res.statusCode = 404
    // are equivalent, however with the option we
    // get the "status" local available as well
    return res.render('notfound', {
        title:"ページが見つかりません",
        message:null,
        data:null,
        session:req.session
    });
});



//*************************** サーバーを開始 ***************************//


var server = http.createServer(app);//サーバーを作成
server.listen(app.get('port'), function(){
    var addr = server.address();
    console.log("Express server listening at", addr.address + ":" + addr.port);
});



//*************************** 例外処理 ***************************//
// 追加。catchされなかった例外の処理設定。
process.on('uncaughtException', function (err) {
    //保存先の設定
    var today = new Date();
    var filepath = './ErrorLogs/'+today.getFullYear()+'_'+(today.getMonth()+1)+'_'+today.getDate()+'.txt';
    if(!fs.existsSync(filepath)){fs.openSync(filepath, 'w');}//無かったら作成
    lwl.logFile = filepath;
    //スタックを表示する。
    lwl.emerg(err.stack + '\n');
    console.log(err.stack);    
    process.exit();
});





